#!/bin/sh

cat >> /etc/apt/sources.list <<EOF

#debian repos
deb http://archive.debian.org/debian jessie main
deb http://archive.debian.org/debian jessie-backports main
deb http://security.debian.org/ jessie/updates main
EOF

cat >> /etc/apt/apt.conf <<EOF
Acquire::Check-Valid-Until "false";
EOF

apt-get update --allow-unauthenticated
sudo apt-get install --allow-unauthenticated cumulus-archive-keyring

apt update -y
apt install -y python nfs-common
