#!/bin/bash

set -x

cat >/tmp/sources.list << EOF
deb http://mirrors.kernel.org/debian/ buster main non-free contrib
deb-src http://mirrors.kernel.org/debian/ buster main non-free contrib

deb http://security.debian.org/debian-security buster/updates main contrib non-free
deb-src http://security.debian.org/debian-security buster/updates main contrib non-free
EOF

sudo cp /tmp/sources.list /etc/apt/sources.list
